const playwright = require('playwright');
const random_useragent = require('random-useragent')
const fs = require('fs')

const BASE_URL = 'https://www.phenomena-experience.com/programacion-mensual/todo.html'

const scrap = async () => {
    // Create user agent
    const agent = random_useragent.getRandom()

    //Set up the browser
    const browser = await playwright.chromium.launch({headless: true})
    const context = await browser.newContext({ userAgent: agent })
    const page = await context.newPage({ bypassCSP: true })
    page.setDefaultTimeout(30000)
    await page.setViewportSize({width:800, height:600})
    await page.goto(BASE_URL)

    // Get the data from the site
    const repo = await page.$$eval('div.event-fichacontent', (repoCards) => {
        return repoCards.map((card) => {
            const [title, details] = card.querySelectorAll('div a')

            const formatText = (element) => element && element.innerText.trim()
            return {
                title: formatText(title),
                details: formatText(details),
                url: title.href
            }
        })
    })

    // Save the data into the file
    const logger = fs.createWriteStream('data.txt', { flag: 'w'})
    logger.write(JSON.stringify(repo, null, ' '))

    // Close the browser
    await browser.close()
    }
scrap().catch(error => {
    console.log(error)
    process.exit(1)
})