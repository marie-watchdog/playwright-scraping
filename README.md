This is a simple web page scraper for the cinema in my neighborhood, 
the data then used in a separate project to send me an updates about new releases.

### Requirements:
node v21.1.0

playwright v1.40.1

random-useragent v0.5.0

### To run it locally:
```
npm install
npm run scraper
```

The results will appear in the _data.txt_ file in the root of the project.